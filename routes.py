"""
Route handling and business logic.
MVP
Elisabeth Finkel; created 2019-06-19; modified 2019-06-20 (Header dates in CST. Business logic time in UTC.)
https://5tvw5wvm2l.execute-api.us-east-1.amazonaws.com/dev/api/options.html
"""
"""
Status codes:
200 - OK        GET -resource
https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
"""
from flask import request, redirect, Flask
import json

app = Flask(__name__)


NEW_SITE = 'https://x9gctr0aac.execute-api.us-east-1.amazonaws.com/dev/api/options.html' # v5

# @app.route('/')
# @app.route('/api/')
@app.route('/api/options.html')
def options():
    code = '<html>'
    code += '<head><title>Go to new site</title></head><body>'
    code += "<a href='" + NEW_SITE + "'>Go to new site</a>"
    code += '</body></html>'
    return code, 200


# @app.route('/api/new/')
# def go_to_new():
#     return redirect(NEW_SITE, code=302)

@app.errorhandler(404)
def page_not_found(e):
    return e404()

def e404():
    return json.dumps({'success': False, 'error': 'Page not found. Website permanently moved to '+NEW_SITE}), 404





"""
Launch app (mostly deprecated - use "sls wsgi serve" instead)
"""
# Will not be true when hosted as AWS Lambda
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
